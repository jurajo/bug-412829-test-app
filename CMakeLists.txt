cmake_minimum_required(VERSION 3.0)
project(KMW_App)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
# Instruct CMake to create code from Qt designer ui files
set(CMAKE_AUTOUIC ON)

# Find the QtWidgets library
find_package(Qt5Widgets CONFIG REQUIRED)

# Find KMessageAddons library for KMessageWidget
find_package(KF5WidgetsAddons)

set(kmw_app_SRC
  src/main.cpp
  src/kmw_app.cpp
)

# Tell CMake to create the helloworld executable
add_executable(kmw_app ${kmw_app_SRC})

# Use the Widgets module from Qt 5.
target_link_libraries(kmw_app
    Qt5::Widgets
    KF5::WidgetsAddons
)

# Install the executable
install(TARGETS kmw_app DESTINATION bin)
