#ifndef KMW_APP_H
#define KMW_APP_H

#include <QMainWindow>
#include <QScopedPointer>

namespace Ui {
class KMW_App;
}

class KMW_App : public QMainWindow
{
    Q_OBJECT

public:
    explicit KMW_App(QWidget *parent = nullptr);
    ~KMW_App() override;

public slots:
    void showShortMessage();
    void showMediumMessage();
    void showLargeMessage();
    void showHugeMessage();

    void showAboutMessage();

private:
    QScopedPointer<Ui::KMW_App> m_ui;
};

#endif // KMW_APP_H
