#include "kmw_app.h"
#include "ui_kmw_app.h"

#include <QMessageBox>


KMW_App::KMW_App(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::KMW_App)
{
    m_ui->setupUi(this);

    m_ui->kmessagewidget->hide();

    connect(m_ui->actionShort, &QAction::triggered, this, &KMW_App::showShortMessage);
    connect(m_ui->actionMedium, &QAction::triggered, this, &KMW_App::showMediumMessage);
    connect(m_ui->actionLarge, &QAction::triggered, this, &KMW_App::showLargeMessage);
    connect(m_ui->actionHuge, &QAction::triggered, this, &KMW_App::showHugeMessage);

    connect(m_ui->actionAbout, &QAction::triggered, this, &KMW_App::showAboutMessage);
}

KMW_App::~KMW_App() = default;

void KMW_App::showShortMessage()
{
    m_ui->kmessagewidget->setText("Mauris mollis dui eget posuere ve, cum.");
    m_ui->kmessagewidget->animatedShow();
}

void KMW_App::showMediumMessage()
{
    m_ui->kmessagewidget->setText("Quisque. Justo rhoncus sodales duis at porttitor lacus hendrerit augue at fringilla fames porttitor.");
    m_ui->kmessagewidget->animatedShow();
}

void KMW_App::showLargeMessage()
{
    m_ui->kmessagewidget->setText("Cursus at duis faucibus venenatis consectetuer gravida nec. Montes ipsum placerat eu, condimentum odio, duis euismod ad. Urna vitae litora ve torquent erat mus curae.");
    m_ui->kmessagewidget->animatedShow();
}

void KMW_App::showHugeMessage()
{
    m_ui->kmessagewidget->setText("Erat conubia luctus, nisi in amet penatibus cursus, maecenas vitae magna orci. Pellentesque. Integer nam, semper a, inceptos dui. Tempor ultrices morbi eleifend ipsum a nibh parturient at, tristique condimentum, est. Venenatis eget sed diam vestibulum.");
    m_ui->kmessagewidget->animatedShow();
}

void KMW_App::showAboutMessage()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Something about This little application");
    msgBox.setText(
        "This application intends to create an environment to test <strong>KMessageWidget</strong> bug "
        "<a href='https://bugs.kde.org/show_bug.cgi?id=412829'>412829</a>"
    );
    msgBox.setInformativeText(
        "<h3>Usage:</h3><br>"
        "Click on the four available actions on <em>toolbar</em> or in <em>Messages</em> menu.<br>"
        "Each of the actions (buttons) will insert lorem ipsum text of different size:<br>"
        "Short (A), Medium (S), Large (D) and Huge (F).<br>"
        "Each of these are to show the bug in it`s full glory.<br/><br/>"
        "Observe this beauty."
    );
    msgBox.exec();
}
