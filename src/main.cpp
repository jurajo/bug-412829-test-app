#include "kmw_app.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    KMW_App w;
    w.show();

    return app.exec();
}

